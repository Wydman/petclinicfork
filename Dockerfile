FROM maven:slim as build
WORKDIR /src
COPY pom.xml .
RUN mvn dependency:go-offline
COPY . /src
RUN mvn spring-javaformat:apply
RUN mvn package
FROM java
COPY --from=build /src/target/*.jar .
EXPOSE 8080
CMD java -jar *.jar
